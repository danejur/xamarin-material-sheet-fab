﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace MaterialSheetFab
{
    public interface IAnimatedFab
    {
        /// <summary>
        /// Shows the FAB.
        /// </summary>
        void Show();

        /// <summary>
        /// Shows the FAB and sets the FAB's translation.
        /// </summary>
        /// <param name="translationX">Translation X value</param>
        /// <param name="translationY">Translation Y value</param>
        void Show(float translationX, float translationY);

        /// <summary>
        /// Hides the FAB.
        /// </summary>
        void Hide();
    }
}