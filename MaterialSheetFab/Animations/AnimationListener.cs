﻿namespace MaterialSheetFab.Animations
{
    public abstract class AnimationListener
    {
        /// <summary>
        /// Called when the animation starts.
        /// </summary>
        public abstract void OnStart();

        /// <summary>
        /// Called when the animation ends.
        /// </summary>
        public abstract void OnEnd();
    }
}