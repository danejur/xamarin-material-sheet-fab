﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using Android.Animation;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Lang;
using Java.Lang.Reflect;
using Exception = System.Exception;

namespace MaterialSheetFab.Animations
{
    public class MaterialSheetAnimation
    {
        private const string SupportCardViewClassName = "android.support.v7.widget.CardView";
        private const int SheetRevealOffsetY = 5;

        private readonly View _sheet;
        private readonly int _sheetColor;
        private readonly int _fabColor;
        private readonly ITimeInterpolator _interpolator;
        private readonly Method _setCardBackgroundColor;
        private readonly bool _isSupportCardView;

        public MaterialSheetAnimation(View sheet, int sheetColor, int fabColor, ITimeInterpolator interpolator)
        {
            _sheet = sheet;
            _sheetColor = sheetColor;
            _fabColor = fabColor;
            _interpolator = interpolator;
            RevealXDirection = MaterialSheetFab.RevealXDirection.Left;
            RevealYDirection = MaterialSheetFab.RevealYDirection.Up;
            _isSupportCardView = sheet.Class.Name.Equals(SupportCardViewClassName, StringComparison.CurrentCultureIgnoreCase);

            if (!_isSupportCardView)
            {
                return;
            }

            try
            {
                _setCardBackgroundColor = sheet.Class.GetDeclaredMethod("SetCardBackgroundColor", Class.FromType(typeof(int)));
            }
            catch
            {
                _setCardBackgroundColor = null;
            }
        }

        public void AlignSheetWithFab(View fab)
        {
            var fabCoords = new int[2];
            fab.GetLocationOnScreen(fabCoords);

            var sheetCoords = new int[2];
            _sheet.GetLocationOnScreen(sheetCoords);

            var leftDiff = sheetCoords[0] - fabCoords[0];
            var rightDiff = (sheetCoords[0] + _sheet.Width) - (fabCoords[0] + fab.Width);
            var topDiff = sheetCoords[1] - fabCoords[1];
            var bottomDiff = (sheetCoords[1] + _sheet.Height) - (fabCoords[1] + fab.Height);

            var sheetLayoutParams = _sheet.LayoutParameters as ViewGroup.MarginLayoutParams;

            if (rightDiff != 0)
            {
                var sheetX = _sheet.GetX();

                if (rightDiff <= sheetX)
                {
                    _sheet.SetX(sheetX - rightDiff - sheetLayoutParams?.RightMargin ?? 0);
                    RevealXDirection = MaterialSheetFab.RevealXDirection.Left;
                }
                else if (leftDiff != 0 && leftDiff <= sheetX)
                {
                    _sheet.SetX(sheetX - leftDiff + sheetLayoutParams?.LeftMargin ?? 0);
                    RevealXDirection = MaterialSheetFab.RevealXDirection.Right;
                }
            }

            if (bottomDiff != 0)
            {
                var sheetY = _sheet.GetY();

                if (bottomDiff <= sheetY)
                {
                    _sheet.SetY(sheetY - bottomDiff - sheetLayoutParams?.BottomMargin ?? 0);
                    RevealYDirection = MaterialSheetFab.RevealYDirection.Up;
                }
                else if (topDiff != 0 && topDiff <= sheetY)
                {
                    _sheet.SetY(sheetY - topDiff + sheetLayoutParams?.TopMargin ?? 0);
                    RevealYDirection = MaterialSheetFab.RevealYDirection.Down;
                }
            }
        }

        public void MorphFromFab(View fab, long showSheetDuration, long showSheetColorDuration,
            AnimationListener listener)
        {
            _sheet.Visibility = ViewStates.Visible;
            RevealSheetWithFab(fab, GetFabRevealedRadius(fab), SheetRevealRadius, showSheetDuration, _fabColor,
                _sheetColor, showSheetColorDuration, listener);
        }

        public void MorphIntoFab(View fab, long hideSheetDuration, long hideSheetColorDuration,
            AnimationListener listener)
        {
            RevealSheetWithFab(fab, SheetRevealRadius, GetFabRevealedRadius(fab), hideSheetDuration, _sheetColor,
                _fabColor, hideSheetColorDuration, listener);
        }

        protected void RevealSheetWithFab(View fab, float startRadius, float endRadius, long sheetDuration,
            int startColor, int endColor, long sheetColorDuration, AnimationListener listener)
        {
            listener?.OnStart();

            var revealListener = sheetDuration >= sheetColorDuration ? listener : null;
            var colorListener = sheetColorDuration > sheetDuration ? listener : null;

            StartCircularRevealAnimation(_sheet, SheetRevealCenterX, GetSheetRevealCenterY(fab),
                startRadius, endRadius, sheetDuration, _interpolator, revealListener);
            StartColorAnimation(_sheet, startColor, endColor, sheetColorDuration, _interpolator, colorListener);
        }

        protected void StartCircularRevealAnimation(View view, int centerX, int centerY, float startRadius,
            float endRadius, long duration, ITimeInterpolator interpolator, AnimationListener listener)
        {
            var relativeCenterX = (int) (centerX - view.GetX());
            var relativeCenterY = (int) (centerY - view.GetY());

            var anim = ViewAnimationUtils.CreateCircularReveal(view, relativeCenterX, relativeCenterY, startRadius,
                endRadius);
            anim.SetDuration(duration);
            anim.SetInterpolator(interpolator);
            anim.AnimationStart += (sender, args) => listener?.OnStart();
            anim.AnimationEnd += (sender, args) => listener?.OnEnd();

            anim.Start();
        }

        protected void StartColorAnimation(View view, int startColor, int endColor, long duration,
            ITimeInterpolator interpolator, AnimationListener listener)
        {
            var anim = ValueAnimator.OfObject(new ArgbEvaluator(), startColor, endColor);
            anim.SetDuration(duration);
            anim.SetInterpolator(interpolator);
            anim.AnimationStart += (sender, args) => listener?.OnStart();
            anim.AnimationEnd += (sender, args) => listener?.OnEnd();

            anim.Update += (sender, args) =>
            {
                var color = (int) (sender as ValueAnimator)?.AnimatedValue;

                if (_isSupportCardView)
                {
                    _setCardBackgroundColor?.Invoke(_sheet, color);
                }
                else
                {
                    view.SetBackgroundColor(new Color(color));
                }
            };

            anim.Start();
        }

        public ViewStates SheetVisibility
        {
            get => _sheet.Visibility;
            set => _sheet.Visibility = value;
        }

        public bool IsSheetVisible => _sheet.Visibility == ViewStates.Visible;

        public int SheetRevealCenterX => (int) (_sheet.GetX() + (_sheet.GetY() / 2));

        public int GetSheetRevealCenterY(View fab)
        {
            if (RevealYDirection == MaterialSheetFab.RevealYDirection.Up)
            {
                return (int) (_sheet.GetY() + _sheet.Height * (float) (SheetRevealOffsetY - 1) / SheetRevealOffsetY -
                              (float) fab.Height / 2);
            }

            return (int) (_sheet.GetY() + (float) _sheet.Height / SheetRevealOffsetY + (float) fab.Height / 2);
        }

        protected float SheetRevealRadius => System.Math.Max(_sheet.Width, _sheet.Height);

        protected float GetFabRevealedRadius(View fab) => (float) System.Math.Max(fab.Width, fab.Height) / 2;

        public MaterialSheetFab.RevealXDirection RevealXDirection { get; private set; }

        public MaterialSheetFab.RevealYDirection RevealYDirection { get; private set; }
    }
}