﻿using Android.Animation;
using Android.Views;

namespace MaterialSheetFab.Animations
{
    public class OverlayAnimation
    {
        protected View Overlay;
        protected ITimeInterpolator Interpolator;

        public OverlayAnimation(View overlay, ITimeInterpolator interpolator)
        {
            Overlay = overlay;
            Interpolator = interpolator;
        }

        public void Show(long duration, AnimationListener listener)
        {
            Overlay.Animate().Alpha(1).SetDuration(duration).SetInterpolator(Interpolator)
                .SetListener(new OverlayAnimatorListenerAdapter(Overlay, listener, true))
                .Start();
        }

        public void Hide(long duration, AnimationListener listener)
        {
            Overlay.Animate().Alpha(0).SetDuration(duration).SetInterpolator(Interpolator)
                .SetListener(new OverlayAnimatorListenerAdapter(Overlay, listener, false))
                .Start();
        }

        private class OverlayAnimatorListenerAdapter : AnimatorListenerAdapter
        {
            private readonly View _overlay;
            private readonly AnimationListener _listener;
            private readonly bool _isVisible;

            public OverlayAnimatorListenerAdapter(View overlay, AnimationListener listener, bool isVisible)
            {
                _overlay = overlay;
                _listener = listener;
                _isVisible = isVisible;
            }

            public override void OnAnimationStart(Animator animation)
            {
                if (_isVisible)
                {
                    _overlay.Visibility = ViewStates.Visible;
                }

                _listener?.OnStart();
            }

            public override void OnAnimationEnd(Animator animation)
            {
                if (!_isVisible)
                {
                    _overlay.Visibility = ViewStates.Gone;
                }

                _listener?.OnEnd();
            }
        }
    }
}