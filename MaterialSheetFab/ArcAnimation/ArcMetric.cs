﻿using System;
using Android.Graphics;

namespace MaterialSheetFab.ArcAnimation
{
    public class ArcMetric
    {
        private readonly PointF _startPoint = new PointF();
        private readonly PointF _endPoint = new PointF();
        private readonly PointF _midPoint = new PointF();
        private readonly PointF[] _axisPoint = new PointF[2];
        private readonly PointF _zeroPoint = new PointF();

        private float _startEndSegment;
        private float _radius;
        private float _midAxisSegment;
        private float _zeroStartSegment;

        private float _animationDegree;
        private float _sideDegree;
        private float _zeroStartDegree;
        private float _startDegree;
        private float _endDegree;

        private Side _side;

        private void CreateAxisVariables()
        {
            for (var i = 0; i < _axisPoint.Length; i++)
            {
                _axisPoint[i] = new PointF();
            }
        }

        private void CalculateStartEndSegment() => _startEndSegment =
            (float) Math.Sqrt(Math.Pow(_startPoint.X - _endPoint.X, 2) + Math.Pow(_startPoint.Y - _endPoint.Y, 2));

        private void CalculateRadius() => _radius = _startEndSegment / Utils.Sin(_animationDegree) *
                                                    Utils.Sin(_sideDegree = (180 - _animationDegree) / 2);

        private void CalculateMidAxisSegment() => _midAxisSegment = _radius * Utils.Sin(_sideDegree);

        private void CalculateMidPoint()
        {
            _midPoint.X =
                 _startPoint.X + _startEndSegment / 2 * (_endPoint.X - _startPoint.X) / _startEndSegment;
            _midPoint.Y =
                 _startPoint.Y + _startEndSegment / 2 * (_endPoint.Y - _startPoint.Y) / _startEndSegment;
        }

        private void CalculateAxisPoints()
        {
            if (_startPoint.Y >= _endPoint.Y)
            {
                _axisPoint[0].X =
                     _midPoint.X + _midAxisSegment * (_endPoint.Y - _startPoint.Y) / _startEndSegment;
                _axisPoint[0].Y =
                     _midPoint.Y - _midAxisSegment * (_endPoint.X - _startPoint.X) / _startEndSegment;

                _axisPoint[1].X =
                     _midPoint.X - _midAxisSegment * (_endPoint.Y - _startPoint.Y) / _startEndSegment;
                _axisPoint[1].Y =
                     _midPoint.Y + _midAxisSegment * (_endPoint.X - _startPoint.X) / _startEndSegment;
            }
            else
            {
                _axisPoint[0].X =
                     _midPoint.X - _midAxisSegment * (_endPoint.Y - _startPoint.Y) / _startEndSegment;
                _axisPoint[0].Y =
                     _midPoint.Y + _midAxisSegment * (_endPoint.X - _startPoint.X) / _startEndSegment;

                _axisPoint[1].X =
                     _midPoint.X + _midAxisSegment * (_endPoint.Y - _startPoint.Y) / _startEndSegment;
                _axisPoint[1].Y =
                     _midPoint.Y - _midAxisSegment * (_endPoint.X - _startPoint.X) / _startEndSegment;
            }
        }

        private void CalculateZeroPoint()
        {
            switch (_side)
            {
                case Side.Right:
                    _zeroPoint.X =  _axisPoint[(int) Side.Right].X + _radius;
                    _zeroPoint.Y = _axisPoint[(int) Side.Right].Y;
                    break;
                case Side.Left:
                    _zeroPoint.X =  _axisPoint[(int) Side.Left].X - _radius;
                    _zeroPoint.Y = _axisPoint[(int) Side.Left].Y;
                    break;
            }
        }

        private void CalculateDegrees()
        {
            _zeroStartSegment =
                (float) Math.Sqrt(Math.Pow(_zeroPoint.X - _startPoint.X, 2) + Math.Pow(_zeroPoint.Y - _startPoint.Y, 2));
            _zeroStartDegree = Utils.Acos((float) ((2 * Math.Pow(_radius, 2) - Math.Pow(_zeroStartSegment, 2)) /
                                                   (2 * Math.Pow(_radius, 2))));

            switch (_side)
            {
                case Side.Right:
                    if (_startPoint.Y <= _zeroPoint.Y)
                    {
                        if (_startPoint.Y > _endPoint.Y ||
                            _startPoint.Y == _endPoint.Y && _startPoint.X > _endPoint.X)
                        {
                            _startDegree = _zeroStartDegree;
                            _endDegree = _startDegree + _animationDegree;
                        }
                        else
                        {
                            _startDegree = _zeroStartDegree;
                            _endDegree = _startDegree - _animationDegree;
                        }
                    }
                    else if (_startPoint.Y >= _zeroPoint.Y)
                    {
                        if (_startPoint.Y < _endPoint.Y ||
                            _startPoint.Y == _endPoint.Y && _startPoint.X > _endPoint.X)
                        {
                            _startDegree = 0 - _zeroStartDegree;
                            _endDegree = _startDegree - _animationDegree;
                        }
                        else
                        {
                            _startDegree = 0 - _zeroStartDegree;
                            _endDegree = _startDegree + _animationDegree;
                        }
                    }

                    break;
                case Side.Left:
                    if (_startPoint.Y <= _zeroPoint.Y)
                    {
                        if (_startPoint.Y > _endPoint.Y ||
                            _startPoint.Y == _endPoint.Y && _startPoint.X < _endPoint.X)
                        {
                            _startDegree = 180 - _zeroStartDegree;
                            _endDegree = _startDegree - _animationDegree;
                        }
                        else
                        {
                            _startDegree = 180 - _zeroStartDegree;
                            _endDegree = _startDegree + _animationDegree;
                        }
                    }
                    else if (_startPoint.Y >= _zeroPoint.Y)
                    {
                        if (_startPoint.Y < _endPoint.Y ||
                            _startPoint.Y == _endPoint.Y && _startPoint.X < _endPoint.X)
                        {
                            _startDegree = 180 + _zeroStartDegree;
                            _endDegree = _startDegree + _animationDegree;
                        }
                        else
                        {
                            _startDegree = 180 + _zeroStartDegree;
                            _endDegree = _startDegree - _animationDegree;
                        }
                    }

                    break;
            }
        }

        public static ArcMetric Evaluate(float startX, float startY,
            float endX, float endY,
            float degree, Side side)
        {
            // TODO: return ready to use object with have done computations
            var arcMetric = new ArcMetric();
            arcMetric._startPoint.Set(startX, startY);
            arcMetric._endPoint.Set(endX, endY);
            arcMetric.SetDegree(degree);
            arcMetric._side = side;
            arcMetric.CreateAxisVariables();

            arcMetric.CalculateStartEndSegment();
            arcMetric.CalculateRadius();
            arcMetric.CalculateMidAxisSegment();
            arcMetric.CalculateMidPoint();
            arcMetric.CalculateAxisPoints();
            arcMetric.CalculateZeroPoint();
            arcMetric.CalculateDegrees();

            return arcMetric;
        }

        public void SetDegree(float degree)
        {
            while (true)
            {
                degree = Math.Abs(degree);
                if (degree > 180)
                {
                    degree = degree % 180;
                    continue;
                }

                if (degree == 180)
                {
                    degree = degree - 1;
                    continue;
                }

                if (degree < 30)
                {
                    degree = 30;
                    continue;
                }

                _animationDegree = degree;

                break;
            }
        }

        public PointF AxisPoint => _axisPoint[(int) _side];

        public float StartDegree => _startDegree;

        public float EndDegree => _endDegree;

        public float Radius => _radius;
    }
}