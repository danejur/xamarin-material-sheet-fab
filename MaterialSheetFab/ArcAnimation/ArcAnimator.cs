﻿using System;
using Android.Animation;
using Android.Views;

namespace MaterialSheetFab.ArcAnimation
{
    public class ArcAnimator : Animator
    {
        private ArcMetric _arcMetric;
        private WeakReference<View> _target;
        private WeakReference<ValueAnimator> _animator;
        private float _value;

        public float Degree => _value;

        private ArcAnimator(ArcMetric arcMetric, View target)
        {
            _arcMetric = arcMetric;
            _target = new WeakReference<View>(target);
            _animator = new WeakReference<ValueAnimator>(ValueAnimator.OfFloat(arcMetric.StartDegree,
                arcMetric.EndDegree));

            if (_animator.TryGetTarget(out var weakTarget))
            {
                weakTarget.AddUpdateListener(new ArcAnimatorUpdateListener(this));
            }
        }

        private void SetDegree(float degree)
        {
            _value = degree;
            if (_target.TryGetTarget(out var clipView))
            {
                var x = _arcMetric.AxisPoint.X + _arcMetric.Radius * Utils.Cos(degree);
                var y = _arcMetric.AxisPoint.Y - _arcMetric.Radius * Utils.Sin(degree);
                clipView.SetX(x - (float)clipView.Width / 2);
                clipView.SetY(y - (float)clipView.Height / 2);
            }
        }

        public static ArcAnimator CreateArcAnimator(View clipView, View nestView, float degree, Side side)
        {
            return CreateArcAnimator(clipView, Utils.CenterX(nestView), Utils.CenterY(nestView), degree, side);
        }

        public static ArcAnimator CreateArcAnimator(View clipView, float endX, float endY, float degree, Side side)
        {
            var arcMetric = ArcMetric.Evaluate(Utils.CenterX(clipView), Utils.CenterY(clipView), endX, endY, degree,
                side);
            return new ArcAnimator(arcMetric, clipView);
        }

        public override long StartDelay
        {
            get => _animator.TryGetTarget(out var a) ? a.Duration : 0;
            set
            {
                if (_animator.TryGetTarget(out var a))
                {
                    a.StartDelay = value;
                }
            }
        }

        public override Animator SetDuration(long duration)
        {
            if (_animator.TryGetTarget(out var a))
            {
                a.SetDuration(duration);
            }

            return this;
        }

        public override long Duration => _animator.TryGetTarget(out var a) ? a.Duration : 0;

        public override void SetInterpolator(ITimeInterpolator value)
        {
            if (_animator.TryGetTarget(out var a))
            {
                a.SetInterpolator(value);
            }
        }

        public override void Start()
        {
            base.Start();
            if (_animator.TryGetTarget(out var a))
            {
                a.Start();
            }
        }

        public override void End()
        {
            base.End();
            if (_animator.TryGetTarget(out var a))
            {
                a.End();
            }
        }

        public override void Cancel()
        {
            base.Cancel();
            if (_animator.TryGetTarget(out var a))
            {
                a.Cancel();
            }
        }

        public override void AddListener(IAnimatorListener listener)
        {
            base.AddListener(listener);
            if (_animator.TryGetTarget(out var a))
            {
                a.AddListener(listener);
            }
        }

        public override void SetupEndValues()
        {
            base.SetupEndValues();
            if (_animator.TryGetTarget(out var a))
            {
                a.SetupEndValues();
            }
        }

        public override void SetupStartValues()
        {
            base.SetupStartValues();
            if (_animator.TryGetTarget(out var a))
            {
                a.SetupStartValues();
            }
        }

        public override bool IsRunning => _animator.TryGetTarget(out var a) && a.IsRunning;

        #region Internal

        private class ArcAnimatorUpdateListener : Java.Lang.Object, ValueAnimator.IAnimatorUpdateListener
        {
            private readonly ArcAnimator _animator;

            public ArcAnimatorUpdateListener(ArcAnimator animator)
            {
                _animator = animator;
            }

            public void OnAnimationUpdate(ValueAnimator animation)
            {
                _animator.SetDegree((float)animation.AnimatedValue);
            }
        }

        #endregion
    }
}