﻿using System;
using Android.Views;

namespace MaterialSheetFab.ArcAnimation
{
    public class Utils
    {
        public static float Sin(float degree) => (float)Math.Sin(ToRadians(degree));

        public static float Cos(float degree) => (float)Math.Cos(ToRadians(degree));

        public static float Asin(float degree) => (float)Math.Asin(ToRadians(degree));

        public static float Acos(float degree) => (float)Math.Acos(ToRadians(degree));

        public static float CenterX(View view) => view.GetX() + (float)view.Width / 2;

        public static float CenterY(View view) => view.GetY() + (float)view.Height / 2;

        private static float ToRadians(float deg) => (float)Math.PI / 180 * deg;
    }
}