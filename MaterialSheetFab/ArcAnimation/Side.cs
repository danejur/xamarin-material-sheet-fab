﻿namespace MaterialSheetFab.ArcAnimation
{
    public enum Side
    {
        Right = 0,
        Left = 1
    }
}