﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace MaterialSheetFab
{
    public abstract class MaterialSheetFabEventListener
    {
        /// <summary>
        /// Called when the material sheet's "show" animation starts.
        /// </summary>
        public abstract void OnShowSheet();

        /// <summary>
        /// Called when the material sheet's "show" animation ends.
        /// </summary>
        public abstract void OnSheetShown();

        /// <summary>
        /// Called when the material sheet's "hide" animation starts.
        /// </summary>
        public abstract void OnHideSheet();

        /// <summary>
        /// Called when the material sheet's "hide" animation ends.
        /// </summary>
        public abstract void OnSheetHidden();
    }
}