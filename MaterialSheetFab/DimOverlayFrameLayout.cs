﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace MaterialSheetFab
{
    public class DimOverlayFrameLayout : FrameLayout
    {
        protected DimOverlayFrameLayout(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        public DimOverlayFrameLayout(Context context) : base(context)
        {
            Init();
        }

        public DimOverlayFrameLayout(Context context, IAttributeSet attrs) : base(context, attrs)
        {
            Init();
        }

        public DimOverlayFrameLayout(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr)
        {
            Init();
        }

        public DimOverlayFrameLayout(Context context, IAttributeSet attrs, int defStyleAttr, int defStyleRes) : base(context, attrs, defStyleAttr, defStyleRes)
        {
            Init();
        }

        private void Init()
        {
            Inflate(Context, Resource.Layout.DimOverlay, this);
        }
    }
}